#pragma once
enum class NonperishableProductType
{
	Clothing = 0,
	SmallAppliences,
	PersonalHygiene
};