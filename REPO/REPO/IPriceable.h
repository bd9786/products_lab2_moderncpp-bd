#pragma once
#include <cstdint>

struct IPriceble
{
	virtual uint8_t GetVAT() const = 0; // const-ul nu permite modificarea lui this (obj curent), care este un parametru implicit
	virtual float GetPrice() const = 0;
};
