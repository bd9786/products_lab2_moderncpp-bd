#include "PerishableProduct.h"

const float PerishableProduct::m_VAT = 9;

PerishableProduct::PerishableProduct(const int32_t id, const std::string& name, const float rawPrice, const std::string& expirationDate) :
	Product(id, name, rawPrice), m_expirationDate(expirationDate)
{
	// empty
}

const std::string& PerishableProduct::GetExpirationDate() const
{
	return m_expirationDate;
}

uint8_t PerishableProduct::GetVAT() const
{
	return this->m_VAT;
}

float PerishableProduct::GetPrice() const
{
	return static_cast<float>(m_rawPrice + m_VAT * 0.01 * m_rawPrice);
}
