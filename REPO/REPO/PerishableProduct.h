#include <cstdint>
#include <string>
#include "IPriceable.h"
#include "Product.h"

class PerishableProduct : public Product
{
public:
	PerishableProduct(const int32_t id, const std::string& name, const float rawPrice, const std::string& m_expirationDate);
	const std::string& GetExpirationDate() const;
	uint8_t GetVAT() const override;
	float GetPrice() const override;

private:
	std::string m_expirationDate;
	static const float m_VAT;
};

