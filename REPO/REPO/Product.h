#pragma once
#include <cstdint>
#include <string>
#include "IPriceable.h"

class Product : public IPriceble
{
public:
	Product(const uint32_t id, const std::string& name, const float rawPrice);
	uint32_t GetID() const;
	const std::string& GetName() const;
	float GetRawPrice() const;

protected:
	 uint32_t m_id;
	 std::string m_name;
	 float m_rawPrice;
};