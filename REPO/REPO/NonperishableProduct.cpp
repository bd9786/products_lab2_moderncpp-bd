#include "NonperishableProduct.h"

NonperishableProduct::NonperishableProduct(const int32_t id, const std::string& name, const float rawPrice, NonperishableProductType& type) :
	Product(id, name, rawPrice), m_type(type)
{
	// empty
}

NonperishableProductType NonperishableProduct::GetType() const
{
	return m_type;
}

uint8_t NonperishableProduct::GetVAT() const
{
	return m_VAT;
}

float NonperishableProduct::GetPrice() const
{
	return static_cast<float>(m_rawPrice + m_VAT * 0.01 * m_rawPrice);
}
