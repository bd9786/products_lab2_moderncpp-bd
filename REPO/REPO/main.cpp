#include "Product.h"
#include "IPriceable.h"
#include "PerishableProduct.h"
#include "NonperishableProduct.h"
#include "NonperishableProductType.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

bool isDateString(const std::string& expirationDate)
{
	return expirationDate.find_first_not_of("0123456789-") == std::string::npos;
	// function that finds first element that is not in the passed string by checking all positions of the input string
	// return value of find_first_not_of : the position of the first character that does not match. If no such characters are found, the function returns string::npos
	// npos as a return value, it is usually used to indicate no matches
}

void sortByPrice(std::vector<Product*>& vectorOfProducts)
{
	std::sort(vectorOfProducts.begin(), vectorOfProducts.end(), [](Product* prod1, Product* prod2) { return prod1->GetRawPrice() < prod2->GetRawPrice() ? true : false; });
	// lambda expression for comp
}

void sortByName(std::vector<Product*>& vectorOfProducts)
{
	std::sort(vectorOfProducts.begin(), vectorOfProducts.end(), [](Product* prod1, Product* prod2) { return prod1->GetName() < prod2->GetName() ? true : false; });
	// lambda expression for comp
}

NonperishableProductType toProductType(const std::string& dateOrType) {
	if (dateOrType == "Clothing") {
		return NonperishableProductType::Clothing;
	}
	else if (dateOrType == "SmallAppliences") {
		return NonperishableProductType::SmallAppliences;
	}
	else if (dateOrType == "PersonalHygiene") {
		return NonperishableProductType::PersonalHygiene;
	}
	throw "Cannot convert to NonPerishableProductType.";
}

int main()
{
	std::vector<Product*> vectorOfProducts;
	int32_t id;
	std::string name;
	float rawPrice;
	std::string dateOrType;
	uint16_t vat;

	for (std::ifstream inputFile("Products.proddb"); !inputFile.eof();)
	{
		inputFile >> id >> name >> rawPrice >> vat >> dateOrType;

		if (isDateString(dateOrType))
		{
			PerishableProduct* obj_perishableProduct = new PerishableProduct(id, name, rawPrice, dateOrType);
			vectorOfProducts.push_back(obj_perishableProduct);
		}
		else
		{
			NonperishableProductType type = toProductType(dateOrType);

		/*	if (dateOrType.compare(std::to_string(Clothing)))
			{
				type = Clothing;
			}
			else if (dateOrType.compare(std::to_string(SmallAppliences)))
			{
				type = SmallAppliences;
			}
			else
			{
				type = PersonalHygiene;
			}
			*/

			NonperishableProduct* obj_nonPerishableProduct = new NonperishableProduct(id, name, rawPrice, type);
			vectorOfProducts.push_back(obj_nonPerishableProduct);
		}

	}

	std::cout << "Vector of products:\n";

	for (auto& it : vectorOfProducts)
	{
		std::cout << it->GetID() << " " << it->GetName() << " " << it->GetRawPrice() << " " << it->GetVAT() << " " << it->GetPrice() << " \n";
	}

	std::cout << "\nThe nonperishable products are:\n";

	for (auto& it : vectorOfProducts)
	{
		if (dynamic_cast<NonperishableProduct*> (it))
		{
			std::cout << it->GetID() << " " << it->GetName() << " " << it->GetPrice() << " \n";
		}
	}

	std::cout << "\nProducts sorted by their full price ( raw price + VAT ):\n";

	sortByPrice(vectorOfProducts);

	for (auto& it : vectorOfProducts)
	{
		std::cout << it->GetID() << " " << it->GetName() << " " << it->GetPrice() << " \n";
	}

	std::cout << "\nProducts sorted by their name:\n";

	sortByName(vectorOfProducts);

	for (auto& it : vectorOfProducts)
	{
		std::cout << it->GetID() << " " << it->GetName() << " \n";
	}

	for (Product* product : vectorOfProducts) {
		delete product;
	}
	return 0;
}